package com.example.dimon.lesson6_widget;

import android.Manifest;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Arrays;
import java.util.Timer;

import static android.content.Context.LOCATION_SERVICE;


/**
 * Created by Dimon on 17.01.2017.
 */

public class CoordinateWidget extends AppWidgetProvider implements LocationListener {

    private LocationManager myManager;

    final String LOG_TAG = "myLogs";

    private AppWidgetManager widgetManager = null;

    /**
     * All widgets ids.
     */
    int[] widgetIds = null;

    /**
     * The timer for updated time.
     */
    private Timer timer = null;

    /**
     * The current context.
     */
    private Context context = null;

    /**
     * The link to preference for quick access.
     */
    private SharedPreferences options = null;

    /**
     * Called when widget need to be updated.
     */


    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));
        // Store context
        this.context = context;

        // Shared preference
        options = context.getSharedPreferences(WidgetConfig.WIDGET_PREF, Context.MODE_PRIVATE);

        // Store widget manager
        widgetManager = appWidgetManager;

        // Cloning all ids for future work
        widgetIds = appWidgetIds.clone();

        // настроим провайдер gps
        myManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        myManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, this); // дистанция каждые пять метров, опрос раз в секунду



        super.onUpdate(context, appWidgetManager, appWidgetIds);

    }


    @Override
    public void onLocationChanged(Location location) {
        updateWidgets(location);


    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    /**
     * Обновляет виджеты.
     *
     * @param location
     */
    private void updateWidgets(Location location) {

        // Update all widgets
        for (int i = 0; i < widgetIds.length; i++) {

            // Create view
            RemoteViews v = new RemoteViews(context.getPackageName(),
                    R.layout.widget);

            String coordinatesText = "Latitude: " + location.getLatitude() + "\n" +
                    "Longtitude: " + location.getLongitude() + "\n" +
                    "Altitude: " + location.getAltitude();

            v.setTextViewText(R.id.tv, coordinatesText);

            // Get color from preference and set it
            int color = options.getInt(String.valueOf(widgetIds[i]), -1);
            if (color != -1) v.setTextColor(R.id.tv, color);

            // Update widget
            widgetManager.updateAppWidget(widgetIds[i], v);

        }


    }


    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        if (options == null) return;
        SharedPreferences.Editor editor = options.edit();
        for (int id : appWidgetIds) {
            editor.remove(String.valueOf(id));
        }
        editor.commit();
    }


    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);

    }

}